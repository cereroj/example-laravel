@extends('welcome')

@section('content')
<div class="menu-tienda">
    @include('menu')
</div>
        <!--  -->
<div class="row">
@if(Session::has('success'))
			<div class="alert alert-info m-2 mb-0">
				{{Session::get('success')}}
			</div>
			@endif
@if($all->count())  
              @foreach($all as $tienda)  
    <div class="col-4 mt-5">
        <div class="card" style="width: 25rem;">
        <div class="card-header text-center">
           <img class="card-img-top m-auto"   src="{{ asset('file/img/'.$tienda->img_url) }}">
           <!-- src=" {{asset($tienda->img_url) }}" -->
        </div>
        <div class="card-body">
          <h5 class="card-title">{{ ucfirst(trans($tienda->nombre_tienda)) }}</h5>
          <ul class="list-group list-group-flush">
          <li class="list-group-item m-0 p-0"></li>
            <li class="list-group-item"><i class="bi bi-geo-alt"></i> {{$tienda->ubicacion_tienda}}</li>
            <li class="list-group-item"><i class="bi bi-telephone"></i> {{$tienda->telefono_tienda}}</li>
            <li class="list-group-item"><i class="bi bi-at"></i>{{$tienda->email_tienda}}</li>
            <li class="list-group-item text-center">
                <div class="btn-group" role="group" aria-label="Basic outlined example">
       
                <!-- <a href="{{ route('tienda.destroy',$tienda->id) }}">  -->
                <form action="{{action('Tienda@destroy', $tienda->id)}}"  role="form" method="post" class="m-0 p-0">
                @csrf
             @method('DELETE')
                  <button type="submit" class="btn btn-danger"><i class="bi bi-trash"></i></button>
                </form>
                <!-- </a> -->
                 <a href="{{action('Tienda@edit', Crypt::encrypt( $tienda->id))}}"> 
                    <button type="button" class="btn btn-success"><i class="bi bi-pencil"></i></button>
                    </a>
                </div>
            </li>
          </ul>
          <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
        </div>
        </div>
    </div>
 
@endforeach 
</div>
               @else
               <div class="m-2 p-2">    <h3>No hay tiendas registradas</h3></div>
              @endif
 @endsection

        

      



