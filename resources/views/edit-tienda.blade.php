@extends('welcome')
@section('content')
@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
<form action="{{ route('tienda.update',$tienda->id) }}"  role="form" method="post" enctype="multipart/form-data" class="mt-3 p-2">
{{ csrf_field() }}
<input name="_method" type="hidden" value="PATCH">
    <div class="form-group">
      <div class="row">
          <div class="mb-3 col-5">
            <label for="formFileSm" class="form-label">Nombre</label>
            <input class="form-control" type="text" id="formFile" name="nombre_tienda"  value="{{$tienda->nombre_tienda}}">
          </div>
          <div class="mb-3 col-5">
            <label for="formFileSm" class="form-label">Ubicacion</label>
            <input class="form-control" type="text" id="formFileMultiple" name="ubicacion_tienda" value="{{$tienda->ubicacion_tienda}}">
          </div>
          <div class="mb-3 col-5">
            <label for="formFileSm" class="form-label">Telefono</label>
            <input class="form-control" type="text" id="formFileDisabled" name="telefono_tienda"  value="{{$tienda->telefono_tienda}}">
          </div>
          <div class="mb-3 col-5">
            <label for="formFileSm" class="form-label">Email</label>
            <input class="form-control form-control-sm" id="formFileSm" type="email" name="email_tienda" value="{{$tienda->email_tienda}}">
          </div>
          <div class="mb-3 col-5">
            <!-- <label for="formFileSm" class="form-label">Img Actual</label> -->
            <img class="card-img-top m-auto" src="{{ asset('file/img/'.$tienda->img_url) }}" alt="Card image cap">
          </div>
          <div class="mb-3 col-5">
            <label for="formFileSm" class="form-label">Img</label>
            <input class="form-control form-control-sm" id="formFileSm" type="file" name="img_tienda">
          </div>
          <div class="mb-3">
          <button type="submit" class="btn btn-success">Send</button>
          </form>
          <a href="{{ route('tienda.index') }}"> 
          <button type="button" class="btn btn-secondary">Atras</button>
          </a>
          </div>
         
      </div>
    </div>

@endsection
