<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model_Tienda;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use League\Flysystem\Filesystem as File;

class Tienda extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $all = Model_Tienda::all();
     
        // var_dump($all);
        return view('list-tiendas',compact('all'))
                     ->with('title', 'Directorio de Tiendas');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // $all->title     = "Registro de Tiendas";
        return view('registro-tienda')
        ->with('title', 'Registro de Tiendas');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $tienda=new Model_Tienda;
        $this->validate($request,[ 
            'nombre_tienda'=>'required', 'ubicacion_tienda'=>'required', 
            'telefono_tienda'=>'required', 'email_tienda'=>'required']);
            
        $tienda->nombre_tienda=$request->nombre_tienda;
        $tienda->ubicacion_tienda =$request->ubicacion_tienda;
        $tienda->telefono_tienda=$request->telefono_tienda;
        $tienda->email_tienda =$request->email_tienda;
        // $tienda->ubicacion_tienda =$request->email_user;
        if ($request->hasFile('img_tienda')) {
            $file = $request->file("img_tienda");
          
            $extension = $request->file('img_tienda')->extension();
    
            $name_img = str_replace(' ','_',$tienda->nombre_tienda);
            $safeName =  $file->getClientOriginalName();
        
            Storage::disk('img')->putFileAs('', $file,$name_img.'.'. $extension);
            $tienda->img_url =   $name_img.'.'. $extension ;
           
        }

      
     
        $tienda->save();

        // 
        // Libro::create($request->all());
        // return redirect()->route('libro.index')->with('success','Registro creado satisfactoriamente');
        return redirect()->route('tienda.index')->with('success','Registro creado satisfactoriamente'); 


          
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id =  Crypt::decrypt($id);
        // var_dump($id);
        $tienda=Model_Tienda::find($id);
        return view('edit-tienda',compact('tienda'))
        ->with('title', 'Edicion de Tienda');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 
            'nombre_tienda'=>'required', 'ubicacion_tienda'=>'required', 
            'telefono_tienda'=>'required', 'email_tienda'=>'required']);
            $tienda = Model_Tienda::find($id);
            // $folderName = public_path().'/file/img';
            if(   Storage::disk('img')->exists($tienda->img_url)){
                Storage::disk('img')->delete($tienda->img_url);
            }
            $tienda->nombre_tienda=$request->nombre_tienda;
            $tienda->ubicacion_tienda =$request->ubicacion_tienda;
            $tienda->telefono_tienda=$request->telefono_tienda;
            $tienda->email_tienda =$request->email_tienda;
            if ($request->hasFile('img_tienda')) {
                $file = $request->file("img_tienda");
                $extension = $request->file('img_tienda')->extension();
                $name_img = str_replace(' ','_',$tienda->nombre_tienda);
                $safeName =  $file->getClientOriginalName();
                Storage::disk('img')->putFileAs('', $file,$name_img.'.'. $extension);
                $tienda->img_url =   $name_img.'.'. $extension ;
               
            }
          
         
            $tienda->save();
            return redirect()->route('tienda.index')->with('success','Tienda actualizada satisfactoriamente');
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Model_Tienda::find($id)->delete();
        return redirect()->route('tienda.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
